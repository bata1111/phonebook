package com.example.demo.Controller;

import com.example.demo.Interface.ContactFilter;
import com.example.demo.Interface.MyContactInterface;
import com.example.demo.Interface.RepositoryInterface;
import com.example.demo.model.MyContact;
import com.example.demo.model.Repository;
import lombok.SneakyThrows;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.jsoup.Jsoup;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "contacts")
public class ControllerContact {
    @Autowired
    private MyContactInterface myContactInterface;
    @Autowired
    private RepositoryInterface repositoryInterface;


    @PutMapping
    public ResponseEntity<Object> add(@RequestBody MyContact newOne) {
        try {
            Optional<MyContact> existedObject = myContactInterface.findById(newOne.getId());
            if (existedObject.isPresent()) {
                return new ResponseEntity<>("this id is earlier exist for update call update method", HttpStatus.BAD_REQUEST);
            }
            newOne = myContactInterface.save(newOne);
            if (newOne.getGithub() != null) {
                getRepo(newOne.getGithub(), newOne);
            }
            return new ResponseEntity<>(newOne, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    //    *************************************************************************************************************
    @PostMapping(value = "search")
    public ResponseEntity<Object> search(@RequestBody MyContact search) {
        try {
            Specification<MyContact> spec = new ContactFilter(search);
            List<MyContact> list = myContactInterface.findAll(spec, PageRequest.of(0, 100, Sort.by(Sort.Direction.ASC, "id"))).getContent();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @SneakyThrows
    private void getRepo(String repoName, MyContact myContact) {
        String next = "";
        if (repoName.startsWith("http")) {
            next = repoName.replace("%3D", "=");
        } else {
            next = "https://github.com/" + repoName + "?tab=repositories";
        }
        try {
            do {
                Document doc = Jsoup.connect(next).get();
                Elements user_repositories_list = doc.getElementById("user-repositories-list").
                        getElementsByTag("ul");
                if (user_repositories_list.size() > 0) {
                    user_repositories_list = doc.getElementById("user-repositories-list").
                            getElementsByTag("ul").get(0).getElementsByTag("li");
                } else {
                    break;
                }
                Elements paginate_container = doc.getElementById("user-repositories-list").
                        getElementsByClass("paginate-container").get(0).getElementsByTag("a");
                if (paginate_container.size() == 1) {
                    String text = paginate_container.get(0).html();
                    if (text.equalsIgnoreCase("next")) {
                        next = doc.getElementById("user-repositories-list").getElementsByClass("paginate-container")
                                .get(0).getElementsByTag("a").get(0).attr("href").replace("%3D", "=");
                    } else {
                        next = "";
                    }
                } else {
                    next = doc.getElementById("user-repositories-list").getElementsByClass("paginate-container")
                            .get(0).getElementsByTag("a").get(1).attr("href").replace("%3D", "=");
                }
                for (Element element : user_repositories_list) {
                    String name = element.getElementsByTag("div").get(0).getElementsByTag("div").get(0)
                            .getElementsByTag("h3").get(0).getElementsByTag("a").get(0).html();
                    String url = element.getElementsByTag("div").get(0).getElementsByTag("div").get(0)
                            .getElementsByTag("h3").get(0).getElementsByTag("a").get(0).attr("href");
                    Repository repository = new Repository(name, "https://github.com" + url, myContact);
                    repositoryInterface.save(repository);
                }
            } while (!next.equals(""));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
