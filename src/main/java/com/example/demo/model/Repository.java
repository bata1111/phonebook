package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;

@Entity(name = "Contact")
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Repository {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @URL
    private String url;
    private String description;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "myContactId", referencedColumnName = "id")
    @ToString.Exclude
    private MyContact myContact;

    public Repository(String description, @URL String url,MyContact myContact) {
        this.url = url;
        this.description = description;
        this.myContact = myContact;
    }
}
