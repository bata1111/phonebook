package com.example.demo.Interface;

import com.example.demo.model.Repository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RepositoryInterface extends PagingAndSortingRepository<Repository, Long> {
}
